import './App.css';
import {Route, Routes} from "react-router-dom";
import React from "react";
import DefaultLayout from "./layouts/DefaultLayout";
import Screen1 from "./pages/Screen1";
import Screen2 from "./pages/Screen2";
import MainScreen from "./pages/MainScreen";
import Screen3 from "./pages/Screen3";
import Screen4 from "./pages/Screen4";


function App() {
    return (
        <div className="App">
            <Routes>
                <Route path="/" element={<DefaultLayout><MainScreen /></DefaultLayout>} />
                <Route path="/list1" element={<DefaultLayout><Screen1 /></DefaultLayout>} />
                <Route path="/list2" element={<DefaultLayout><Screen2 /></DefaultLayout>} />
                <Route path="/list3" element={<DefaultLayout><Screen3 /></DefaultLayout>} />
                <Route path="/list4" element={<DefaultLayout><Screen4 /></DefaultLayout>} />
            </Routes>
        </div>
    );
}

export default App;
