import React from 'react';
import {Link} from "react-router-dom";

const DefaultLayout = ({ children }) => {
    return (
        <>
            <div>
                header
                <nav>
                    <Link to ="/list1">목록1</Link>
                    <Link to ="/list2">목록2</Link>
                    <Link to ="/list3">목록3</Link>
                    <Link to ="/list4">목록4</Link>
                </nav>
            </div>
            <main>{children}</main>
            <footer>footer</footer>
        </>
    )
}

export default DefaultLayout;