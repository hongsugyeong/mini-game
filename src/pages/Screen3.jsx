import React, {useState} from 'react';

const Screen3 = () => {
    const BOX_PRICE = 5000

    const [money, setMoney] = useState(50000)
    const [history, setHistory] = useState([])

    const items = [
        {itemName: "짱구 피규어", percent: 0.01},
        {itemName: "철수 피규어", percent: 0.02},
        {itemName: "맹구 피규어", percent: 0.02},
        {itemName: "훈이 피규어", percent: 0.02},
        {itemName: "유리 피규어", percent: 0.02},
        {itemName: "배우경 피규어", percent: 0.91},
    ]

    // 아이템 랜덤 돌리기
    const getRandomItems = () => {
        const random = Math.random()
        let sum = 0
        for (const item of items) {
            sum += item.percent
            if (random < sum) return item
        }
    }

    // 오픈을 클릭하면 경품 보여주기, 돈 차감
    const openClick = () => {
        if (BOX_PRICE < money) {
            const item = getRandomItems()
            setMoney(money - 5000)
            setHistory([...history, item.itemName])
        } else {
            alert('돈 벌어서 오세요')
        }
    }

    const replayClick = () => {
        setMoney(money + 50000)
    }

    return (
        <div>
            <h1>갸차</h1>
            <p>돈 현황 : {money}원</p>
            <button onClick={openClick}>오픈</button>
            <p>보유 현황 : {history.map((item, index) => (
                <li key={index}>{item}</li>
            ))}</p>
            <button onClick={replayClick}>돈 충전</button>
        </div>
    )
}

export default Screen3