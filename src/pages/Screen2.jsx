import React, {useState} from 'react';

const Screen2 = () => {
    const [topClothes, setTopClothes] = useState('')
    const [bottomClothes, setBottomClothes] = useState('')
    const [pickTop, setPickTop] = useState('')
    const [pickBottom, setPickBottom] = useState('')

    const handleTopCheck = e => {
        setTopClothes(e.target.value)
    }

    const handleBottomCheck = e => {
        setBottomClothes(e.target.value)
    }

    const clothesCheck = () => {
        const topArr = topClothes.split(',')
        const bottomArr = bottomClothes.split(',')

        setPickTop(topArr[Math.floor(Math.random() * topArr.length)])
        setPickBottom(bottomArr[Math.floor(Math.random() * bottomArr.length)])
    }

    return (
        <div>
            <h1>슈의 옷가게</h1>
            상의 : <input type="text" value={topClothes} onChange={handleTopCheck}/><br/>
            하의 : <input type="text" value={bottomClothes} onChange={handleBottomCheck}/>
            <br/>
            <br/>
            <button onClick={clothesCheck}>결정</button>
            <div>
                내일은 {pickTop}에 {pickBottom} 입으셈
            </div>
        </div>
    )
}

export default Screen2;