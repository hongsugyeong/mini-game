import React, {useState} from 'react';

const Screen1 = () => {
    const [userName, setUserName] = useState('')
    const [partnerName, setPartnerName] = useState('')
    const [score, setScore] = useState(0)

    const handleUserName = e => {
        setUserName(e.target.value)
    }

    const handlePartnerName = e => {
        setPartnerName(e.target.value)
    }

    const calculateScore = () => {
        setScore(Math.floor(Math.random() * 101))
    }

    return (
        <div>
            <h1>넌 나고 난 너야</h1>
            <input type="text" value={userName} onChange={handleUserName}/>
            <input type="text" value={partnerName} onChange={handlePartnerName}/>
            <br/>
            <br/>
            <button type='submit' onClick={calculateScore}>궁합 확인하기</button>
            <p>{userName}과(와) {partnerName}의 라브러부!? {score}%</p>
        </div>
    )
}

export default Screen1