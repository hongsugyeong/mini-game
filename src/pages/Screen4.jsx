import React, {useState} from 'react';

const Screen4 = () => {
    const [money, setMoney] = useState(0);
    const [userA, setUserA] = useState('');
    const [userB, setUserB] = useState('');
    const [userC, setUserC] = useState('');
    const [userPrice , setUserPrice] = useState([])

    const inputMoney = e => {
        setMoney(e.target.value)
    }

    const inputUserA = e => {
        setUserA(e.target.value)
    }

    const inputUserB = e => {
        setUserB(e.target.value)
    }

    const inputUserC = e => {
        setUserC(e.target.value)
    }

    // 돈을 나눠야
    const sliceMoney = () => {
        // 퍼센트로 처음부터 계산

        const p1 = Math.floor(Math.random() * 101)
        const p2 = Math.floor(Math.random() * (101-p1))
        const p3 = 100 - p1 - p2

        console.log(p1, p2, p3)

        setUserPrice([Math.floor((p1 * 0.01) *  money),Math.floor((p2 * 0.01) *  money) ,Math.floor((p3 * 0.01) *  money)])
    }

    return (
        <div>
            <h1>돈 낸하</h1>
            금액 : <input type="number" value={money} onChange={inputMoney} />
            <br/>
            <br/>
            이름 : <input type="text" value={userA} onChange={inputUserA}/>
            <br/>
            이름 : <input type="text" value={userB} onChange={inputUserB}/>
            <br/>
            이름 : <input type="text" value={userC} onChange={inputUserC}/>
            <br/>
            <br/>
            <button onClick={sliceMoney}>나누기</button>
            <p>{userA} : {userPrice[0]} {userB}: {userPrice[1]} {userC}: {userPrice[2]}</p>
        </div>
    )
}

export default Screen4;