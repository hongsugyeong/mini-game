<img src="/public/img/react.jpeg">

# 미니게임 천국

## 프로젝트 간단 설명
Open [http://localhost:3000](http://localhost:3000)

간단하고 재미있는 미니 게임을 즐겨보세요. <br>
시간 가는 줄 모르고 즐기시게 될 것입니다.


### 프로젝트 실행 화면
<img src="/public/img/mini-game-main-screen.png">
게임 시작 전 메인 화면입니다. <br>
위의 목록을 선택하여 미니 게임을 즐기실 수 있습니다. <br><br>

#### 첫번째 미니 게임 : 넌 나고 난 너야 <br>
<img src="/public/img/mini-game-screen1.png">
당신의 이름과 상대방의 이름을 적으면 궁합도를 측정해줍니다. <br>
당장 서로의 궁합도를 확인해주세요! <br><br>

#### 두번째 미니 게임 : 슈의 옷가게
<img src="/public/img/mini-game-screen2.png">
내일 입을 옷을 결정하지 못 하셨나요? <br>
옷이 너무 많아 결정을 못 하시겠나요? <br>
걱정하지 마세요~ 당신이 입을 옷을 슈가 결정해주겠습니다. <br>
새로운 조합의 옷을 기대해보세요!  <br><br>

#### 세번째 미니 게임 : 가챠, 랜덤 뽑기
<img src="/public/img/mini-game-screen3-1.png">
어릴 적 많이 했던 뽑기 게임 기억하시나요? <br>
이제 온라인으로 즐겨보세요 <br>
행운이 가득하다면 당신은 짱구 피규어를 획득할 수 있습니다. <br>

<img src="/public/img/mini-game-screen3-2.png">
대신 뽑기는 한 번당 5000원이니 돈이 부족하면 충전을 해주세요!  <br><br>

#### 네번째 미니 게임 : 돈 낸하
<img src="/public/img/mini-game-screen4.png">
회식을 하고 정산하기 어려우셨나요? <br>
재미있게 정산을 하고 싶으신가요? <br>
그렇다면 나누기 버튼을 눌러주세요 <br>
과연 당신은 얼마를 낼까요?  <br><br>

### 사용한 기술
1. 메인 화면
    - 라우터
2. 첫번째 미니 게임 : 넌 나고 난 너야
    - Random
3. 두번째 미니 게임 : 슈의 옷가게
    - split
    - Random
    - Array
4. 세번째 미니 게임 : 가챠, 랜덤 뽑기
    - Random
    - percent
    - ...
5. 네번째 미니 게임 : 돈 낸하
    - Random
    - percent